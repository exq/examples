import ajax from "components/helpers/ajax";
import { log } from "components/helpers/utils";
import { transitionEndEventName } from "components/helpers/prefixes";
import scrollTop from "components/helpers/scrollTop";

class ajaxContent {
  constructor(options = {}){
    
    this.options = {
      element: null,
      path: null,
      scenario: 'open',
      only: null,
      ajax: {
        url: null,
        type: 'POST',
        data: null,
        dataType: 'JSON',
        beforeSend: () => {},
        always: () => {}
      }
    }
    
    this.endTimeOut = 200;
    this.response = null;
    this.queryOld = false;
    
    this.mobilePageIsClose = false;
    
    this.init(options);
    this.initDOM();
  }
  
  
  // -----------------------------
  // Initialize
  // -----------------------------
  
  init(options){
  
    for(let index in options){
      if(index === 'ajax'){
        let ajax = options.ajax;
        
        for(let index in ajax){
          this.options.ajax[index] = ajax[index];
        }
        
      } else {
        this.options[index] = options[index];
      }
    }
    
    this.serialize();
  }

  initDOM(){
    this.DOM = {
      header: document.querySelector('header'),
      page: document.querySelector('.page'),
              
      sideWrap: document.querySelector('.page .side'),
      side: document.querySelector('.page .side__inner'),
      
      contentWrap: document.querySelector('.page .content__box'),
      content: document.querySelector('.page .content__inner'),
    }
  }
  
  start(){
    this._ajaxStart();
  }
  
  setQueryOld(){
    this.queryOld = true;
  }
  
  
  // -----------------------------
  // Ajax
  // -----------------------------
  
  _ajaxStart(){
    new Promise((resolve, reject) => {
      this.updateActiveLink();
      this.ajaxQuery = ajax(this.options.ajax);
      
      if(document.querySelector('.header').classList.contains('is-open'))
        document.querySelector('.header').classList.remove('is-open');
      
      resolve();
    })
    .then(() => {
      
      if(window.circ !== undefined)
        window.circ.pauseAnim();
      
      let result = this.isSuccessScenario(['switch'])
        ? this.onSwitcher()
        : true
      ;
      
      return result;
      
    })
    .then(() => {
    
      let result = this.isSuccessScenario(['open'])
        ? this.openPage()
        : true
      ;
      
      return result;
      
    })
    .then(() => {
    
      let result = this.isSuccessScenario(['open', 'close'])
        ? this.hideContent()
        : true
      ;
      
      return result;
      
    })
    .then(() => { 
      this.showLoader();
      this._ajaxComplete();
    });
  }
  
  // ...
  