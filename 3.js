import "components/helpers/requestAnimationFrame";

/*
 *  *******************************************
 *
 *  FIRST WAY:
 *
 *  let animator = new scrollEvent(
 *    function(event){
 *      console.log(event.positions.current);
 *    }
 *  );
 *  
 *  *******************************************
 *
 *  SECOND WAY:
 *
 *  class childClass extends scrollEvent {
 *
 *    constructor(options = {}){
 *      super()
 *    
 *      this.fn = this.loopFn;
 *      this.loop();
 *     
 *    }
 *   
 *    loopFn(event){
 *      this.print();
 *    }
 
 *    print(){
 *      log('hello');
 *    }
 *  }
 *
 *  *******************************************
*/

class scrollEvent {

  constructor(fn){
    this.lastPosition = -1;
    
    if(fn !== undefined){
      this.fn = fn;
      this.loop();
    }
  }
  
  loop(){
    if(this.lastPosition != window.pageYOffset){
      
      let args = {
        positions: {
          current: window.pageYOffset,
          last: this.lastPosition
        }
      }
      
      this.fn(args, this);
      this.lastPosition = window.pageYOffset;
    }
    
    window.requestAnimationFrame(this.loop.bind(this));
  }
  
}

export default scrollEvent;
