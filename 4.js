export default (args) => {

  let yii = {
    getCsrfParam: () => {
      return document.querySelector('meta[name=csrf-param]').getAttribute('content');
    },
  
    getCsrfToken: () => {
      return document.querySelector('meta[name=csrf-token]').getAttribute('content');
    },
  
    setCsrfToken: (name, value) => {
      document.querySelector('meta[name=csrf-param]').setAttribute('content', name);
      document.querySelector('meta[name=csrf-token]').setAttribute('content', value);
    },
    
    refreshCsrfToken: () => {
      let token = yii.getCsrfToken();
      if (token) {
      
        Array.prototype.forEach.call(
          document.querySelectorAll('form input[name="' + yii.getCsrfParam() + '"]'),
          (item) => {
            item.value = token;
          }
        );
      
      }
    }
  }

  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
  
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          resolve(JSON.parse(xhr.responseText));
        } else {
          reject(xhr.responseText);
        }
      }
    };
    
    xhr.open(args.type, args.url, true);
    
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');  
    xhr.setRequestHeader('X-CSRF-Token', yii.getCsrfToken());
       
    xhr.send(args.data);
    
    yii.refreshCsrfToken();
  });
  
}


// -------------------------------------------


import scrollEvent from "components/helpers/scrollEvent";
import { log } from "components/helpers/utils";

export default () => {
  return new scrollEvent(
    function(event){
    
      let el = document.querySelector('header'),
          windowOffset = 60,
          
          addClass = event.positions.current > windowOffset && !el.classList.contains('is-active'),
          removeClass = event.positions.current <= windowOffset && el.classList.contains('is-active');
      
      if(addClass || removeClass)
        el.classList.toggle('is-active');
      
    }
  );
}