import FastClickComponent from "components/libs/fast-click";

import "components/helpers/requestAnimationFrame";
import { log, addEventListenerAll } from "components/helpers/utils";
import ajax from "components/helpers/ajax";

import mainPageSlider from "components/base/mainPageSlider";
import map from "components/base/map";

import headerFixationOnScroll from "components/OnScroll/headerFixation";

export default () => {
  FastClickComponent();
  
  window.blockKeyboard = false;
  
  // header fixed state toggle
  headerFixationOnScroll();
  
  // window settings
  window.slides = new mainPageSlider();
  window.partial = null;
  window.mobileVersion = false;
  
  checkMobileStatus();
  checkCirclesAnimation();
  
  (() => {
    let
      path = window.location.pathname;
      
    window.history.replaceState({path: path}, document.title, path);
  
    window.addEventListener('popstate', (event) => {
      if(event.state !== null)
        window.location.href = event.state.path;
      else
        window.location.reload();
    });
  })();
  
  function checkMobileStatus(){
    window.innerWidth <= 990
      ? window.mobileVersion = true
      : window.mobileVersion = false
    ;
  }
  
  function checkCirclesAnimation(){
    if(window.circ !== undefined){
      !window.mobileVersion
        ? window.circ.startAnim()
        : window.circ.pauseAnim()
      ;
    }  
  }
  
  window.addEventListener('resize', (event) => {  
    log('resize');
    
    let
      version = window.mobileVersion;
      
    checkMobileStatus();
    checkCirclesAnimation();
    
    if(version !== window.mobileVersion && window.map !== undefined){
      let googleMapOnloadResize = setInterval(() => {
        if (typeof google === 'object' && typeof google.maps === 'object') {
          window.map.initMap();
          clearInterval(googleMapOnloadResize);
        }
      }, 100);
    }
  });
  
};